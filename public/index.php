<?php

//Application initialisation/entry point.
if (isset($_GET['action'])){
    //router
    switch ($_GET['action']){
        case 'list':
            $action = 'list';
            break;

        case 'show':
            $action = 'show';
            break;

        default:
            $action = 'list';
            break;
    }
} else {
    $action = 'list';
}

require '../src/Controller/Controller.php';
require '../src/Model/Manager.php';
require '../src/View/View.php';

$manager = new Manager();
$controller = new Controller($manager);

$controller->{$action}($_REQUEST);
