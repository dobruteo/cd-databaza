<?php

class View{

    public function renderList($albums){
        require "html/list.phtml";
    }

    public function renderItem($item){
        require "html/item.phtml";
    }

    public function renderError($e){
        require "html/error.phtml";
    }
}