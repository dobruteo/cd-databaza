<?php

class Controller{
    private $manager;

    public function __construct($manager){
        $this->manager = $manager;
    }

    //List all albums on main page
    public function list($request){
        $View = new View();
        try{
            $albums = $this->manager->findAll();
        } catch (Exception $e){
            $View->renderError($e);
            return;
        }
        $View->renderList($albums);
    }

    //Show one specific album
    public function show($request){
        $View = new View();
        try{
            $item = $this->manager->find($request['id']);
        } catch (Exception $e){
            $View->renderError($e);
            return;
        }
        $View->renderItem($item);
    }
}