<?php

class Manager{
    private $connectionParam = [
        'host' => '',
        'port' => '',
        'user' => 'root',
        'password' => '',
        'dbname' => 'cd'
    ];
    private $conn;


    //Establishing connection to database
    public function __construct(){
        $this->conn = new mysqli($this->connectionParam['host'], $this->connectionParam['user'],
                                $this->connectionParam['password'], $this->connectionParam['dbname']);
        if ($this->conn->connect_error){
            die("Connection failed: " . $this->conn->connect_error);
        }
        mysqli_set_charset($this->conn, 'utf8');
    }

    //Find all albums in db
    public function findAll(){
        $ret = array();
        $query = "SELECT * FROM albums";

        $result = $this->conn->query($query);

        if (! $result->num_rows > 0){
            throw new Exception("0 results");
        } else {
            while($row = $result->fetch_assoc()) {
                array_push($ret, array("id"=>$row['id'], "author"=>$row['author'], "title"=>$row['title'],
                            "genre"=>$row['genre'], "year"=>$row['year'], "price"=>$row['price'],
                            "rating"=>$row['rating'], "picture"=>$row['picture']));
            }
        }
        return $ret;
    }

    //Find one album by given id
    public function find($id){
        if(!is_numeric($id)){
            throw new Exception("not numeric id");
        }

        $query = "SELECT * FROM albums WHERE id=$id";

        $result = $this->conn->query($query);

        if(! $result->num_rows > 0){
            throw new Exception("id not found");
        } else {
            return $result->fetch_assoc();
        }
    }
}