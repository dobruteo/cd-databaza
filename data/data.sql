
CREATE TABLE `albums` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `year` int(255) NOT NULL,
  `price` float(6,2) DEFAULT NULL,
  `rating` float(2,1) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', 'V rámci oného', 'Rock', '1997', 99, 5, 'HS-v-ramci-oneho.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', 'Vo štvorici po opici', 'Rock', '1998', 89, 4.8, 'HS-vo-stvorici-po-opici.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', 'Ja chaču tebja', 'Rock', '2000', 119, 4.2, 'HS-ja-chacu-tebja.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', 'Festival Chorobná', 'Rock', '2001', 99, 4.9, 'HS-festival-chorobna.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', 'Kýže sliz', 'Rock', '2002', 99, 4.7, 'HS-kyze-sliz.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', 'Alibaba a 40 krátkych songov', 'Rock', '2003', 89, 4.9, 'HS-Alibaba-a-40-kratkych-songov.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', 'Ritero Xaperle Bax', 'Rock', '2004', 89, 4, 'HS-Ritero-Xaperle-Bax.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', 'Ukáž tú tvoju ZOO', 'Rock', '2007', 99, 4.1, 'HS-ukaz-tu-tvoju-zoo.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', '54 dole hlavou', 'Rock', '2009', 129, 3.8, 'HS-54-dole-hlavou.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', 'St. Mary Huana Ganja', 'Rock', '2012', 119, 4.3, 'HS-st-mary-huana-ganja.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('Horkýže Slíže', 'Pustite Karola', 'Rock', '2017', 199, 3.5, 'HS-pustite-karola.webp');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('MIDI LIDI', 'Čekání na robota', 'Electronic', '2007', 89, 4, 'ML-cekani-na-robota.jpg');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('MIDI LIDI', 'Hastmans, Tatrmans & Bubáks', 'Electronic', '2009', 89, 3.9, 'ML-hastrmans-tatrmans-and-bubaks.jpg');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('MIDI LIDI', 'Operace "Kindigo!"', 'Electronic', '2011', 99, 4.9, 'ML-operace-kindigo.jpg');
INSERT INTO albums (author, title, genre, year, price, rating, picture) VALUES ('MIDI LIDI', 'Give Masterpiece A Chance', 'Electronic', '2016', 189, 5, 'ML-give-masterpiece-a-chance.jpg');
